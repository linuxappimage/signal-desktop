# Signal Desktop

Signal Desktop for Linux - AppImage

Download **Signal-x86_64.AppImage** binary from here: [Signal-x86_64.AppImage](https://gitlab.com/linuxappimage/signal-desktop/-/jobs/artifacts/master/raw/Signal-x86_64.AppImage?job=run-build)

or from you command line:

```bash
curl -sLo Signal-x86_64.AppImage https://gitlab.com/linuxappimage/signal-desktop/-/jobs/artifacts/master/raw/Signal-x86_64.AppImage?job=run-build

chmod +x Signal-x86_64.AppImage
./Signal-x86_64.AppImage

```

